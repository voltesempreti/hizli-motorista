package com.hizli.driver.ui.activity.past_detail;


import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
