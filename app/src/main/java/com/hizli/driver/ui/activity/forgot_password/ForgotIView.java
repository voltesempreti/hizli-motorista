package com.hizli.driver.ui.activity.forgot_password;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
