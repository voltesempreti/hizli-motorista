package com.hizli.driver.ui.bottomsheetdialog.invoice_flow;

import com.hizli.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
