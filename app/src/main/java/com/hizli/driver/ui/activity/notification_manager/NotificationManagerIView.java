package com.hizli.driver.ui.activity.notification_manager;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}