package com.hizli.driver.ui.activity.help;


import com.hizli.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
