package com.hizli.driver.ui.activity.wallet;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.WalletMoneyAddedResponse;
import com.hizli.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
