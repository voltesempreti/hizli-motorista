package com.hizli.driver.ui.activity.help;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}
