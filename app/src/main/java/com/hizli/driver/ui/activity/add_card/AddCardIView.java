package com.hizli.driver.ui.activity.add_card;

import com.hizli.driver.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);

    void onError(Throwable e);
}
