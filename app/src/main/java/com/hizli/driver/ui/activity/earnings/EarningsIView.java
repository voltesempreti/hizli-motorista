package com.hizli.driver.ui.activity.earnings;


import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}
