package com.hizli.driver.ui.activity.wallet_detail;

import com.hizli.driver.base.MvpPresenter;
import com.hizli.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
