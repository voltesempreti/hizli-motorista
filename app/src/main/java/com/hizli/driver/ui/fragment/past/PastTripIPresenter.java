package com.hizli.driver.ui.fragment.past;


import com.hizli.driver.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
