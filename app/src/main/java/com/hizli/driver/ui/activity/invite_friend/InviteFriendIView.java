package com.hizli.driver.ui.activity.invite_friend;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
