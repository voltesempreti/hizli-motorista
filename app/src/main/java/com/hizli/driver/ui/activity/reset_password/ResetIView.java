package com.hizli.driver.ui.activity.reset_password;

import com.hizli.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
