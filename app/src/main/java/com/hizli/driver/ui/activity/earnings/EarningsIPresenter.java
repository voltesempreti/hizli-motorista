package com.hizli.driver.ui.activity.earnings;


import com.hizli.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
