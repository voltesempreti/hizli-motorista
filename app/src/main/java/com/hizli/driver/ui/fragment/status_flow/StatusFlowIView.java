package com.hizli.driver.ui.fragment.status_flow;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.TimerResponse;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);

    void onWaitingTimeSuccess(TimerResponse object);

    void onError(Throwable e);
}
