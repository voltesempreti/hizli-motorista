package com.hizli.driver.ui.activity.invite_friend;

import com.hizli.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
