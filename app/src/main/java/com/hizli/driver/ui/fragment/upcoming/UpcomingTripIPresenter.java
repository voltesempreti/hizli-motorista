package com.hizli.driver.ui.fragment.upcoming;


import com.hizli.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
