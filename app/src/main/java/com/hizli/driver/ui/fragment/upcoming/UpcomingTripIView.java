package com.hizli.driver.ui.fragment.upcoming;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
