package com.hizli.driver.ui.activity.summary;


import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}
