package com.hizli.driver.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
