package com.hizli.driver.ui.activity.request_money;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
