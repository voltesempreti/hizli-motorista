package com.hizli.driver.ui.activity.instant_ride;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.EstimateFare;
import com.hizli.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}
