package com.hizli.driver.ui.bottomsheetdialog.rating;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
