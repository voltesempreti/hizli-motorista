package com.hizli.driver.ui.activity.setting;

import com.hizli.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
