package com.hizli.driver.ui.activity.wallet_detail;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}
