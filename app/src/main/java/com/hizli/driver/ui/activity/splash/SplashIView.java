package com.hizli.driver.ui.activity.splash;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.CheckVersion;

public interface SplashIView extends MvpView {

    void verifyAppInstalled();

    void onSuccess(Object user);

    void onSuccess(CheckVersion user);

    void onError(Throwable e);

    void onCheckVersionError(Throwable e);
}
