package com.hizli.driver.ui.activity.sociallogin;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
