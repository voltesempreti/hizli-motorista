package com.hizli.driver.ui.activity.password;

import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.ForgotResponse;
import com.hizli.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}
