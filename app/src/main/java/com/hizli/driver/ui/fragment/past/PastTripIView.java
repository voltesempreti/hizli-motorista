package com.hizli.driver.ui.fragment.past;


import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
