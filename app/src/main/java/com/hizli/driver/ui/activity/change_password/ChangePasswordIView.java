package com.hizli.driver.ui.activity.change_password;

import com.hizli.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
