package com.hizli.driver.ui.activity.upcoming_detail;


import com.hizli.driver.base.MvpView;
import com.hizli.driver.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
