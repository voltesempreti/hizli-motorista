package com.hizli.driver.ui.fragment.offline;

import com.hizli.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
